# Make + Docker + Helm project sample

This project sample shows the usage of _to be continuous_ templates:

* GNU Make (uses [Lazarus 2.2.4](https://www.lazarus-ide.org/) to build and test the project)
* Docker
* Helm ([Flexible Engine - Cloud Container Engine](https://cloud.orange-business.com/en/offers/infrastructure-iaas/public-cloud/features/cloud-container-engine/))

The project exposes a _payroll service_ API developped in [Free Pascal 3.2.2](https://www.freepascal.org/)/[Object Pascal](https://castle-engine.io/modern_pascal_introduction.pdf), packaged as a Docker image, deployed on a Kubernetes cluster (with Helm).

## GNU Make template features

Here are a few highlights about how this project uses the GNU Make template:

* uses a custom Docker image with both `make` and [Lazarus 2.2.4](https://www.lazarus-ide.org/), defined with `MAKE_IMAGE`
* implements unit tests (using [fpcunit](https://wiki.freepascal.org/fpcunit)) with [JUnit reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html) (requires overridding the `.gitlab-ci.yml`)

## Docker template features

This project builds and _pushes_ a Docker image embedding the built application.
For this, a Docker registry is required.
As a matter of fact there are two options (this project implements the first one).

### Option 1: use the GitLab registry

This is the easiest as the Docker template is preconfigured to work this way.

Extra requirements:

1. make sure the Kubernetes cluster has (network) access to the GitLab registry (true in our case through the internet),
2. create a Kubernetes secret with GitLab registry credentials to `docker pull` images:
    ```bash
    kubectl create secret docker-registry gitlab-registry-credentials --docker-server=registry.gitlab.com --docker-username=token --docker-password=$SOME_GITLAB_TOKEN
    ```
3. use those credentials when using images from GitLab, with the `imagePullSecrets` field.

### Option 2: use the Flexible Engine registry

This project could easily use the Kubernetes Cluster's Docker registry (`registry.eu-west-0.prod-cloud-ocb.orange-business.com`).
For this, the following would be required:

1. make sure the GitLab runners have (network) access to the external registry,
2. Override default `$DOCKER_SNAPSHOT_IMAGE` and `$DOCKER_RELEASE_IMAGE` complying to specific [Flexible Engine's policy](https://docs.prod-cloud-ocb.orange-business.com/usermanual/swr/swr_01_0011.html):
    ```yaml
    DOCKER_SNAPSHOT_IMAGE: registry.eu-west-0.prod-cloud-ocb.orange-business.com/to-be-continuous/burger-maker/snapshot:$CI_COMMIT_REF_SLUG
    DOCKER_RELEASE_IMAGE: registry.eu-west-0.prod-cloud-ocb.orange-business.com/to-be-continuous/burger-maker:$CI_COMMIT_REF_NAME
    ```
3. Define :lock: `$DOCKER_REGISTRY_USER` and :lock: `$DOCKER_REGISTRY_PASSWORD` as secret project variables, obtained according [Flexible Engine documentation](https://docs.prod-cloud-ocb.orange-business.com/usermanual/swr/swr_01_1000.html).

## Helm template features

This project uses the following features from the Helm template:

* Overrides the default `kubectl` version to match CCE version by declaring `$HELM_CLI_IMAGE` in the `.gitlab-ci.yml` file, [helm version skew documentation](https://helm.sh/docs/topics/version_skew/) help you to find wich helm version choose according your kubernetes version.
* Defines helm `Chart.yaml` location in `HELM_CHART_DIR`,
* Defines mandatory `HELM_DEFAULT_KUBE_CONFIG`.

The Helm template also implements [environments integration](https://docs.gitlab.com/ee/ci/environments/) in GitLab:

* deployment environment integrated in merge requests,
* review environment cleanup support (manually or when the related feature branch is deleted).

### implementation details

In order to perform Helm deployments, this project implements:

* `Chart.yaml` A YAML file containing information about the chart,
* `values.yaml` The default configuration values for this chart,
* `templates` directory : a directory of templates that, when combined with values will generate valid Kubernetes manifest files.

All those scripts and descriptors make use of [values](https://helm.sh/docs/chart_best_practices/values/) dynamically evaluated and exposed by the Helm template:

* `{{ .Values.hostname }}`: the environment hostname, extracted from the statically defined environment url
* `{{ .Values.config.image }}`: the OCI image that was just built in the upstream pipeline by the CNB template and that is being deployed,
  dynamically injected/substitued in the [values](values.yaml) with the `${cnb_image_digest}` pattern
* `{{ .Values.config.elb_ip }}`: the ELB IP address, dynamically injected/substitued in the [values](values.yaml) with the `${FE_ELB_IP}` 
  pattern (defined as a project variable)

## Getting Started

This chapter presents how to setup your development environment to develop this project locally.

### Prerequisites

In order to start developing payrollService application it's necessary to get Lazarus IDE under any Linux distribution and install dependencies:
1. ZeosLib components from Lazarus Online Package Manager;
2. MariaDB dev libraries;

A Virtual Machine based on openSUSE Linux which already contains them and also other additional tools can be downloaded from [here](http://google.com/openSUSE-Lazarus-2.2.4.ova).


### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/to-be-continuous/samples/freepascal-make-helm.git
   ```
2. Install ZeosLib components from Lazarus Online Package Manager
3. Instal MariaDb dev libraries 
4. Build the application
   ```sh
   make
   ```
