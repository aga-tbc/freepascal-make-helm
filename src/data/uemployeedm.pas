unit uEmployeeDM;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, ZConnection, ZDataset, HTTPDefs, fpjson, jsonparser;

type

  { TEmployee }

  TEmployee = class(TObject)
  private
    FDbConnection: TZConnection;
    FQuery: TZQuery;
  private
    function GetEmployee: TJSONObject;
  public
    function GetEmployeeById(AEmployeeId: string): string;
    function GetEmployees(req: TRequest): string;
    function AddEmployee(req: TRequest): string;
    procedure UpdateEmployee(req: TRequest);
    procedure DeleteEmployee(AEmployeeId: string);
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  uMain, hAppLog, hEmployeeDM, uCommon;

{ TEmployee }

function TEmployee.GetEmployee: TJSONObject;
var
  jObject: TJSONObject;
begin
  Result := TJSONObject.Create();
  if not FQuery.IsEmpty then
  begin
    Result.Add(pfnId, FQuery.FieldByName(dfnId).AsInteger);
    Result.Add(pfnEmployeeNo, FQuery.FieldByName(dfnEmployeeNo).AsString);
    Result.Add(pfnFirstName, FQuery.FieldByName(dfnFirstName).AsString);
    Result.Add(pfnMiddleName, FQuery.FieldByName(dfnMiddleName).AsString);
    Result.Add(pfnLastName, FQuery.FieldByName(dfnLastName).AsString);
    jObject := TJSONObject.Create();
    jObject.Add(pfnId, FQuery.FieldByName(dfnDepartmentId).AsInteger);
    jObject.Add(pfnName, FQuery.FieldByName(dfnDepartmentName).AsString);
    Result.Add(pfnDepartment, jObject);
    jObject := TJSONObject.Create();
    jObject.Add(pfnId, FQuery.FieldByName(dfnPositionId).AsInteger);
    jObject.Add(pfnName, FQuery.FieldByName(dfnPositionName).AsString);
    Result.Add(pfnPosition, jObject);
    Result.Add(pfnSalary, FQuery.FieldByName(dfnSalary).AsCurrency);
  end;
end;

function TEmployee.GetEmployeeById(AEmployeeId: string): string;
var
  jObject: TJSONObject;
  AEmpId: integer;
begin
  Result := '';
  AEmpId := StrToIntDef(AEmployeeId, 0);
  if AEmpId <= 0 then
    raise EClientException.Create('Invalid employee id.');
  if not FDbConnection.Connected then
    FDbConnection.Connect();
  FQuery.Close();
  FQuery.SQL.Clear();
  FQuery.SQL.Add('SELECT');
  FQuery.SQL.Add('e.id, e.employee_no, e.firstname, e.middlename, e.lastname,');
  FQuery.SQL.Add('e.department_id, d.name department_name, e.position_id,');
  FQuery.SQL.Add('p.name position_name, e.salary');
  FQuery.SQL.Add('FROM');
  FQuery.SQL.Add('employee e, department d, position p');
  FQuery.SQL.Add('WHERE');
  FQuery.SQL.Add(Format('e.department_id = d.id AND e.position_id = p.id AND e.id = :%s',
    [pfnEmployeeId]));
  FQuery.Prepare();
  FQuery.ParamByName(pfnEmployeeId).AsInteger := AEmpId;
  FQuery.Open();
  if FQuery.IsEmpty then
    raise ENoDataFoundException.Create(Format('Employee %d doesn''t exists.',
      [AEmpId]));
  jObject := GetEmployee;
  try
    Result := jObject.AsJSON;
  finally
    jObject.Free;
  end;
end;

function TEmployee.GetEmployees(req: TRequest): string;
var
  jArray: TJSONArray;
  AFilters: TStringList;
  I: integer;
  AEmployeeNo, AFirstName, AMiddleName, ALastName, ADepartmentName,
  APositionName: string;
  ADepartmentId, APositionId: integer;
  ASalary: currency;
begin
  AEmployeeNo := req.QueryFields.Values[pfnEmployeeNo];
  AFirstName := req.QueryFields.Values[pfnFirstName];
  AMiddleName := req.QueryFields.Values[pfnMiddleName];
  ALastName := req.QueryFields.Values[pfnLastName];
  ADepartmentId := StrToIntDef(req.QueryFields.Values[pfnDepartment + '.' + pfnId], 0);
  ADepartmentName := req.QueryFields.Values[pfnDepartment + '.' + pfnName];
  APositionId := StrToIntDef(req.QueryFields.Values[pfnPosition + '.' + pfnId], 0);
  APositionName := req.QueryFields.Values[pfnPosition + '.' + pfnName];
  ASalary := StrToCurrDef(req.QueryFields.Values[pfnSalary], 0);

  if not FDbConnection.Connected then
    FDbConnection.Connect();

  FQuery.Close();
  FQuery.SQL.Clear();
  FQuery.SQL.Add('SELECT');
  FQuery.SQL.Add('e.id, e.employee_no, e.firstname, e.middlename, e.lastname,');
  FQuery.SQL.Add('e.department_id, d.name department_name, e.position_id,');
  FQuery.SQL.Add('p.name position_name, e.salary');
  FQuery.SQL.Add('FROM');
  FQuery.SQL.Add('employee e, department d, position p');
  FQuery.SQL.Add('WHERE');
  FQuery.SQL.Add('e.department_id = d.id AND e.position_id = p.id');
  AFilters := TStringList.Create();
  try
    if AEmployeeNo <> '' then
      AFilters.Add(Format('AND e.%s = :%s', [dfnEmployeeNo, pfnEmployeeNo]));
    if AFirstName <> '' then
      AFilters.Add(Format('AND e.%s = :%s', [dfnFirstName, pfnFirstName]));
    if AMiddleName <> '' then
      AFilters.Add(Format('AND e.%s = :%s', [dfnMiddleName, pfnMiddleName]));
    if ALastName <> '' then
      AFilters.Add(Format('AND e.%s = :%s', [dfnLastName, pfnLastName]));
    if ADepartmentId > 0 then
      AFilters.Add(Format('AND e.%s = :%s', [dfnDepartmentId, pfnDepartmentId]));
    if ADepartmentName <> '' then
      AFilters.Add(Format('AND d.%s = :%s', [dfnName, pfnDepartmentName]));
    if APositionId > 0 then
      AFilters.Add(Format('AND e.%s = :%s', [dfnPositionId, pfnPositionId]));
    if APositionName <> '' then
      AFilters.Add(Format('AND p.%s = :%s', [dfnName, pfnPositionName]));
    if ASalary > 0 then
      AFilters.Add(Format('AND e.%s = :%s', [dfnSalary, pfnSalary]));
    for I := 0 to AFilters.Count - 1 do
      FQuery.SQL.Add(AFilters[I]);
  finally
    AFilters.Free();
  end;
  //WriteToStdOut('SQL: ' + FQuery.SQL.Text + LineEnding);
  FQuery.Prepare();
  for I := 0 to FQuery.Params.Count - 1 do
    case FQuery.Params[I].Name of
      pfnEmployeeNo: FQuery.Params[I].AsString := AEmployeeNo;
      pfnFirstName: FQuery.Params[I].AsString := AFirstName;
      pfnMiddleName: FQuery.Params[I].AsString := AMiddleName;
      pfnLastName: FQuery.Params[I].AsString := ALastName;
      pfnDepartmentId: FQuery.Params[I].AsInteger := ADepartmentId;
      pfnDepartmentName: FQuery.Params[I].AsString := ADepartmentName;
      pfnPositionId: FQuery.Params[I].AsInteger := APositionId;
      pfnPositionName: FQuery.Params[I].AsString := APositionName;
      pfnSalary: FQuery.Params[I].AsCurrency := ASalary;
    end;

  FQuery.Open();
  if FQuery.IsEmpty then
    raise ENoDataFoundException.Create('No data found.');
  jArray := TJSONArray.Create();
  try
    while not FQuery.EOF do
    begin
      jArray.Add(GetEmployee);
      FQuery.Next();
    end;
    Result := jArray.AsJSON;
  finally
    jArray.Free;
  end;
end;

function TEmployee.AddEmployee(req: TRequest): string;
var
  rawJson: ansistring;
  employee, jObject: TJSONObject;
  AEmployeeNo, AFirstName, AMiddleName, ALastName{, ADepartmentName,
  APositionName}: string;
  ADepartmentId, APositionId: integer;
  ASalary: currency;
  jData: TJSONData;
begin
  Result := '';

  rawJson := req.Content;
  employee := TJSONObject(GetJSON(rawJson));

  if employee.Find(pfnEmployeeNo, jData) then
    AEmployeeNo := jData.AsString
  else
    raise EClientException.CreateFmt(SFieldNotFound, [pfnEmployeeNo]);

  if employee.Find(pfnFirstName, jData) then
    AFirstName := jData.AsString
  else
    raise EClientException.CreateFmt(SFieldNotFound, [pfnFirstName]);

  if employee.Find(pfnMiddleName, jData) then
    AMiddleName := jData.AsString
  else
    raise EClientException.CreateFmt(SFieldNotFound, [pfnMiddleName]);

  if employee.Find(pfnLastName, jData) then
    ALastName := jData.AsString
  else
    raise EClientException.CreateFmt(SFieldNotFound, [pfnLastName]);

  if employee.Find(pfnDepartment, jObject) then
  begin
    if jObject.Find(pfnId, jData) then
      ADepartmentId := jData.AsInteger
    else
      raise EClientException.CreateFmt(SFieldNotFound, [pfnDepartment + '/' + pfnId]);
  end
  else
    raise EClientException.CreateFmt(SFieldNotFound, [pfnDepartment]);
  //ADepartmentName := employee.FindPath(pfnDepartment).FindPath(pfnName).AsString;

  if employee.Find(pfnPosition, jObject) then
  begin
    if jObject.Find(pfnId, jData) then
      APositionId := jData.AsInteger
    else
      raise EClientException.CreateFmt(SFieldNotFound, [pfnPosition + '/' + pfnId]);
  end
  else
    raise EClientException.CreateFmt(SFieldNotFound, [pfnPosition]);
  //APositionName := employee.FindPath(pfnPosition).FindPath(pfnName).AsString;

  if employee.Find(pfnSalary, jData) then
    ASalary := jData.AsFloat
  else
    raise EClientException.CreateFmt(SFieldNotFound, [pfnSalary]);

  if not FDbConnection.Connected then
    FDbConnection.Connect();

  FQuery.Close();
  FQuery.SQL.Clear();
  FQuery.SQL.Add('INSERT INTO employee');
  FQuery.SQL.Add(
    '(employee_no, firstname, middlename, lastname, department_id, position_id, salary)');
  FQuery.SQL.Add(
    Format('VALUES(:%s, :%s, :%s, :%s, :%s, :%s, :%s) RETURNING id',
    [pfnEmployeeNo, pfnFirstName, pfnMiddleName, pfnLastName,
    pfnDepartmentId, pfnPositionId, pfnSalary]));
  FQuery.Prepare();
  FQuery.ParamByName(pfnEmployeeNo).AsString := AEmployeeNo;
  FQuery.ParamByName(pfnFirstName).AsString := AFirstName;
  FQuery.ParamByName(pfnMiddleName).AsString := AMiddleName;
  FQuery.ParamByName(pfnLastName).AsString := ALastName;
  FQuery.ParamByName(pfnDepartmentId).AsInteger := ADepartmentId;
  FQuery.ParamByName(pfnPositionId).AsInteger := APositionId;
  FQuery.ParamByName(pfnSalary).AsCurrency := ASalary;
  FQuery.Open();
  //FQuery.Close();
  //FQuery.SQL.Text := 'SELECT LAST_INSERT_ID() id';
  //FQuery.Open();
  employee.Add(pfnId, FQuery.FieldByName(dfnId).AsInteger);
  Result := employee.AsJSON;
end;


procedure TEmployee.UpdateEmployee(req: TRequest);
var
  rawJson: ansistring;
  employee, jObject: TJSONObject;
  jData: TJSONData;
  AEmployeeNo, AFirstName, AMiddleName, ALastName: string;
  ADepartmentId, APositionId: integer;
  ASalary: currency;
  AFields: TStringList;
  I: integer;
  S: string;
  AEmpId: integer;
begin
  AEmpId := StrToIntDef(req.RouteParams['employeeId'], 0);
  if AEmpId <= 0 then
    raise EClientException.Create('Invalid employee id.');

  rawJson := req.Content;
  employee := TJSONObject(GetJSON(rawJson));

  AFields := TStringList.Create();
  try
    if employee.Find(pfnEmployeeNo, jData) then
    begin
      AFields.Add(Format('%s=:%s,', [dfnEmployeeNo, pfnEmployeeNo]));
      AEmployeeNo := jData.AsString;
    end;
    if employee.Find(pfnFirstName, jData) then
    begin
      AFields.Add(Format('%s=:%s,', [dfnFirstName, pfnFirstName]));
      AFirstName := jData.AsString;
    end;
    if employee.Find(pfnMiddleName, jData) then
    begin
      AFields.Add(Format('%s=:%s,', [dfnMiddleName, pfnMiddleName]));
      AMiddleName := jData.AsString;
    end;
    if employee.Find(pfnLastName, jData) then
    begin
      AFields.Add(Format('%s=:%s,', [dfnLastName, pfnLastName]));
      ALastName := jData.AsString;
    end;
    if employee.Find(pfnDepartment, jObject) then
    begin
      AFields.Add(Format('%s=:%s,', [dfnDepartmentId, pfnDepartmentId]));
      ADepartmentId := jObject.Find('id').AsInteger;
    end;
    if employee.Find(pfnPosition, jObject) then
    begin
      AFields.Add(Format('%s=:%s,', [dfnPositionId, pfnPositionId]));
      APositionId := jObject.Find('id').AsInteger;
    end;
    if employee.Find(pfnSalary, jData) then
    begin
      AFields.Add(Format('%s=:%s,', [dfnSalary, pfnSalary]));
      ASalary := jData.AsFloat;
    end;
    if AFields.Count = 0 then
      raise EClientException.Create('No valid fields specified.');
    S := AFields[AFields.Count - 1];
    AFields[AFields.Count - 1] := Copy(S, 1, Length(S) - 1);

    if not FDbConnection.Connected then
      FDbConnection.Connect();
    FQuery.Close();
    FQuery.SQL.Clear();
    FQuery.SQL.Add('UPDATE employee');
    FQuery.SQL.Add('SET');
    for I := 0 to AFields.Count - 1 do
      FQuery.SQL.Add(AFields[I]);
  finally
    AFields.Free();
  end;
  FQuery.SQL.Add(Format('WHERE id=:%s', [pfnEmployeeId]));
  FQuery.Prepare();
  for I := 0 to FQuery.Params.Count - 1 do
    case FQuery.Params[I].Name of
      pfnEmployeeNo: FQuery.Params[I].AsString := AEmployeeNo;
      pfnFirstName: FQuery.Params[I].AsString := AFirstName;
      pfnMiddleName: FQuery.Params[I].AsString := AMiddleName;
      pfnLastName: FQuery.Params[I].AsString := ALastName;
      pfnDepartmentId: FQuery.Params[I].AsInteger := ADepartmentId;
      pfnPositionId: FQuery.Params[I].AsInteger := APositionId;
      pfnSalary: FQuery.Params[I].AsCurrency := ASalary;
      pfnEmployeeId: FQuery.Params[I].AsInteger := AEmpId;
    end;
  FQuery.ExecSQL();
  if FQuery.RowsAffected = 0 then
    raise EClientException.Create('No data updated.');
end;

procedure TEmployee.DeleteEmployee(AEmployeeId: string);
var
  AEmpId: integer;
begin
  AEmpId := StrToIntDef(AEmployeeId, 0);
  if AEmpId <= 0 then
    raise EClientException.Create('Invalid employee id.');
  if not FDbConnection.Connected then
    FDbConnection.Connect();
  FQuery.Close();
  FQuery.SQL.Text := (Format('DELETE FROM employee WHERE id=:%s', [pfnEmployeeId]));
  FQuery.Prepare();
  FQuery.ParamByName(pfnEmployeeId).AsInteger := AEmpId;
  FQuery.ExecSQL();
  if FQuery.RowsAffected = 0 then
    raise ENoDataFoundException.Create(Format('Employee %d doesn''t exists.',
      [AEmpId]));
end;

constructor TEmployee.Create;
begin
  inherited Create();
  FDbConnection := TZConnection.Create(nil);
  FQuery := TZQuery.Create(nil);
  FQuery.Connection := FDbConnection;
  with Application.AppSettings.LockAppSettngs() do
    try
      FDbConnection.Protocol := {'pooled.' +} DbConnection.Protocol;
      FDbConnection.HostName := DbConnection.HostName;
      FDbConnection.Port := DbConnection.Port;
      FDbConnection.User := DbConnection.User;
      FDbConnection.Password := DbConnection.Password;
      FDbConnection.Database := DbConnection.Database;
      FDbConnection.Properties.Values['MaxConnections'] :=
        IntToStr(DbConnection.MaxConnections);
      FDbConnection.Properties.Values['Wait'] := BoolToStr(DbConnection.Wait, True);
      FDbConnection.Properties.Values['ConnectionTimeout'] :=
        IntToStr(DbConnection.Timeout);
    finally
      Application.AppSettings.UnlockAppSettings();
    end;
end;

destructor TEmployee.Destroy;
begin
  FreeAndNil(FQuery);
  FreeAndNil(FDbConnection);
  inherited Destroy();
end;

end.
