all: release

release:
	lazbuild --build-mode=Release -B src/*.lpi
	lazbuild -B src/tests/*.lpi

debug:
	lazbuild --build-mode=Debug -B src/*.lpi

.EXPORT_ALL_VARIABLES:
TEST_STRING_ENV_VAR=6Q9YiWesjLzKsbYNet
LOGGING_LOG_LEVEL=INFO
LOGGING_IDENTIFICATION=payrollService
DBCONNECTION_PROTOCOL=MariaDB-10
DBCONNECTION_HOSTNAME=localhost
DBCONNECTION_PORT=3306
DBCONNECTION_USER=payroll
DBCONNECTION_PASSWORD=thepassword
DBCONNECTION_DATABASE=payroll
API_CREDENCIALS=NlE5WWlXZXNqTHpLc2JZTmV0Cg==

test:
	mkdir -p reports
	src/tests/build/UnitTests --all > reports/fpcunit-test.xunit.xml
